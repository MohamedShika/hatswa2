/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.sessionfactory.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Shika
 */
public class HibernateUtil {
    
    private static SessionFactory sessionFactory;

    public HibernateUtil() {
    }

    public static synchronized SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            sessionFactory = new Configuration().configure("iti36/gb/hatswaq/configuration/hibernate/hibernate.cfg.xml").
                    buildSessionFactory();
        }
        return sessionFactory;
    }
    
    public  static void closeSessionFactory(){
        sessionFactory.close();
    }
}
