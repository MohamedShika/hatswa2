/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.dao.interfaces.hibernate;

import iti36.gb.hatswaq.dao.entity.hibernate.Offer;
import java.util.ArrayList;

/**
 *
 * @author Shika
 */
public interface OfferDAO {
    
    public ArrayList<Offer> getOffer(int from,int to);
}
