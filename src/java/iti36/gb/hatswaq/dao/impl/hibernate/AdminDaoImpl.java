/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.dao.impl.hibernate;

import iti36.gb.hatswaq.dao.entity.hibernate.Admin;
import iti36.gb.hatswaq.dao.interfaces.hibernate.AdminDAO;
import iti36.gb.hatswaq.genaric.dao.impl.GenericDAOImpl;
import iti36.gb.hatswaq.sessionfactory.hibernate.HibernateUtil;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Aya
 */
public class AdminDaoImpl extends GenericDAOImpl<Admin> implements AdminDAO{

    @Override
    public Admin checkAdmin(String userName, String password) {
        Admin admin=new Admin();
         Criteria criteria=HibernateUtil.getSessionFactory().openSession().createCriteria(Admin.class)
                .add(Restrictions.and(Restrictions.eq("username",userName),Restrictions.eq("password", password)));
           //     .add(Restrictions.eq("id.productId", 2))
        List result=criteria.list();
        if (result.isEmpty())
            return null;
        else
        {
            admin.setUsername(userName);
            admin.setPassword(password);
            return admin;
        }
    }
}
