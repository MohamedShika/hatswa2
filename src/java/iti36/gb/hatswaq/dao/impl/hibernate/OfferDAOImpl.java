/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.dao.impl.hibernate;

import iti36.gb.hatswaq.dao.entity.hibernate.Offer;
import iti36.gb.hatswaq.dao.interfaces.hibernate.OfferDAO;
import iti36.gb.hatswaq.genaric.dao.impl.GenericDAOImpl;
import iti36.gb.hatswaq.sessionfactory.hibernate.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;

/**
 *
 * @author Shika
 */
public class OfferDAOImpl extends GenericDAOImpl<Offer> implements OfferDAO{

    @Override
    public ArrayList<Offer> getOffer(int from, int to) {
        Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Offer.class);
        criteria.setFirstResult(from);
        criteria.setMaxResults(to);
        List<Offer> offers = criteria.list(); 
        for (Offer offer : offers) {
            System.out.println(offer.getDescription()+" "+offer.getIcon()+" "+offer.getRate());
        }
        return (ArrayList<Offer>) offers;
    }
    
}
