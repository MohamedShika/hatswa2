/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.managedbean;

import iti36.gb.hatswaq.dao.entity.hibernate.Admin;
import iti36.gb.hatswaq.dao.entity.hibernate.Item;
import iti36.gb.hatswaq.dao.impl.hibernate.ItemDAOImpl;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import org.primefaces.event.FileUploadEvent;
import iti36.gb.hatswaq.dao.impl.hibernate.ItemDAOImpl;
import javax.faces.bean.ManagedProperty;
/**
 *
 * @author Aya
 */
@ManagedBean(name="adminItem")
public class AdminItem {
    private List<Item> items;
    private Item item = new Item(null,"", false);
    private DataModel<Item> model;
    private ItemDAOImpl itemImpl = new ItemDAOImpl();
    
    public AdminItem() {
        items = new ArrayList<>();
        items = itemImpl.getAll(item);
        model = new ListDataModel<>(items);
    }

    
    public String updateItem(){
        item = model.getRowData();
        this.item = itemImpl.update(item);
        if(this.item != null) { 
            item = new Item(null, "", false);
            return null;
        }else
            return "error";
    }
    
    public String deactivateItem(){
        item = model.getRowData();
        item.setDeactivate(!item.getDeactivate());
        this.item = itemImpl.update(item);
        if(item != null) { 
            item = new Item(null, "", false);
            return null;
        }else
            return "error";
    }
    
    /**
     * @return the items
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }

    /**
     * @return the item
     */
    public Item getItem() {
        return item;
    }

    /**
     * @param item the item to set
     */
    public void setItem(Item item) {
        this.item = item;
    }

    /**
     * @return the model
     */
    public DataModel<Item> getModel() {
        return model;
    }

    
    public void setModel(DataModel<Item> model) {
        this.model = model;
    }
    
    private String itemName;
    private String itemDescription;
    private String path = "C:\\Users\\Wafik Aziz\\Desktop\\GP project\\pics\\";
    private String fileName;

    public String getItemName() {
        return itemName;
    }

  

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
       
    }
    
    public void handleFileUpload(FileUploadEvent event) {
        fileName = event.getFile().getFileName();
        try {
            copyFile(event.getFile().getFileName(), event.getFile().getInputstream());
        } catch (IOException e) {
            e.printStackTrace();
        }
}
    
    public void copyFile(String fileName, InputStream in) {
           try {
   
                // write the inputStream to a FileOutputStream
                OutputStream out = new FileOutputStream(new File(path + fileName));
              
                int read = 0;
                byte[] bytes = new byte[1024];
              
                while ((read = in.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
              
                in.close();
                out.flush();
                out.close();
              
                System.out.println("New file created!");
                } catch (IOException e) {
                System.out.println(e.getMessage());
                }
    }

    public CheckLogin getCheckLogin() {
        return checkLogin;
    }

    public void setCheckLogin(CheckLogin checkLogin) {
        this.checkLogin = checkLogin;
    }
    
    @ManagedProperty(value="#{checkLogin}")
	private CheckLogin checkLogin;
    
    public void handleClose() {
       Item myItem = new Item();
       myItem.setName(itemName);
       myItem.setDescription(itemDescription);
       myItem.setIcon(fileName);
//       Session session = HibernateUtil.getSessionFactory().openSession();
//       session.beginTransaction();
//       session.persist(myItem);
//       session.
  itemImpl.create(myItem);
  
            }
    
}
