/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.managedbean;

import iti36.gb.hatswaq.dao.entity.hibernate.Admin;
import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import iti36.gb.hatswaq.dao.impl.hibernate.AdminDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.VendorDaoImpl;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Aya
 */

@ManagedBean(name = "checkLogin")
@SessionScoped
public class CheckLogin {
    String userName;
    String password;
    Admin admin;
    Vendor vendor;
    AdminDaoImpl adminDaoImpl;
    VendorDaoImpl vendorDaoImpl;
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String checkUser()
    {
        System.out.println("inside check");
        String page="AdminVendorLogin";
        admin=new Admin();
         vendor=new Vendor();
         adminDaoImpl=new AdminDaoImpl();
         vendorDaoImpl=new VendorDaoImpl();
        admin=adminDaoImpl.checkAdmin(userName, password);
        if(admin!=null)
        {
           page="AdminMainPage";
        }
        vendor=vendorDaoImpl.checkVendor(userName, password);
        if(vendor!=null)
            page="VendorMainPage";
        return page;
    }
}
