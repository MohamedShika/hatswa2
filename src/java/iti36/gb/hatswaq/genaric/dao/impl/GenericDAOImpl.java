/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.genaric.dao.impl;

import iti36.gb.hatswaq.dao.entity.hibernate.Item;
import iti36.gb.hatswaq.genaric.dao.GenericDAOINT;
import iti36.gb.hatswaq.sessionfactory.hibernate.HibernateUtil;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Shika
 */
public abstract class GenericDAOImpl<T> implements GenericDAOINT<T>{
    
//    private Set<T> objects = new HashSet<T>();
    private Class<T> type;
    
    public GenericDAOImpl() {
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        type = (Class) pt.getActualTypeArguments()[0];
    }

    @Override
    public boolean create(T entity) {
        if (entity == null) 
            return false;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(entity);
        session.getTransaction().commit();
        session.close();
        System.out.println("************ "+entity.toString()+" are added.");
        return true;        
    }

    @Override
    public T update(T entity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.merge(entity);
        session.getTransaction().commit();
        session.close();
        System.out.println("************ "+entity.toString()+" are updated.");
        return entity;
    }

    @Override
    public boolean delete(T entity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        try {
            session.delete(entity);
        } catch (Exception ex) {
            return false;
        }
        session.getTransaction().commit();
        session.close();
        System.out.println("************ "+entity.toString()+" are deleted.");
        return true;
    }

    @Override
    public T getByIndex(T entity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria criteria =  session.createCriteria((Class) entity).add(Restrictions.idEq(entity));
        entity = (T) criteria.uniqueResult();
        session.close();
        System.out.println("************ "+entity.toString()+" are updated.");
        return entity;
    }

    @Override
    public List<T> getAll(T entity) {
        Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(entity.getClass());
        HibernateUtil.getSessionFactory().close();
        return criteria.list();
    }
}