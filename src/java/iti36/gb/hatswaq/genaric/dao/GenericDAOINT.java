/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.genaric.dao;

import java.util.List;

/**
 *
 * @author Shika
 * @param <T>
 */
public interface GenericDAOINT<T> {
    
    public boolean create(T entity);

    public T update(T entity);

    public boolean delete(T entity);

    public T getByIndex(T entity);

    public List<T> getAll(T entity);
}
